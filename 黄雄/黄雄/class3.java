import java.util.Scanner;

//用for循环结构,修改第2题，
//在请用户输入成绩前先询问用户一共要输入几门课的成绩。
//例如：如果用户输入5，则只接收5门成绩，若输入8则接收8门成绩
public class class3 {
	public static void main(String[] args) {
		Scanner mn = new Scanner(System.in);
		System.out.println("请输入接收几门课成绩？");
		int a; 
		a = mn.nextInt();
		for (int i = 1; i <=a; i++) {
			System.out.println("第"+i+"门成绩:");
		}
	}
}
