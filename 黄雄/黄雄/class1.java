//用for循环结构
//把100-200之间不能被3整除的数输出。
public  class class1 {
	public static void main(String[] args) {
		System.out.println("100-200之间不能被3整除的数");
		for (int i = 100; i <=200; i++) {
			if (i%3!=0) {
				System.out.print("\t"+i);
			}
		}
	}
}
