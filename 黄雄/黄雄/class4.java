import java.util.Scanner;

//用for循环结构,计算用户输入的数字的阶乘，
//例如用户输入的是5，则计算5的阶乘，
//5的阶乘为 54321
public class class4 {
	public static void main(String[] args) {
		Scanner mn=new Scanner(System.in);
		System.out.println("请输入几的阶乘？");
		int a = mn.nextInt();
		int b =1;
		for (int i = a; i >=1; i--) {
			b = b*i;
		}
		System.out.println("第"+a+"的阶乘:"+b);
	}
}
